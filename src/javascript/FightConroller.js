import Fighter from './Fighter'
class fightController {
  constructor(fightersView) {
    const startButton = document.getElementById('start-button');
    startButton.style.display = 'block';
    document.getElementById('instruction').style.display = 'block';
    document.getElementById('startImg').style.display = 'block';
    startButton.addEventListener('click', () => this._handleClickFight(fightersView));

    this._choosed = fightersView.fight;
  }

  _fight(fighter1, fighter2, callback) {
    this._inizializeHpBars(fighter1, fighter2);
    const health1 = fighter1.health;
    const health2 = fighter2.health;
    const interval = setInterval(() => {
      if (fighter1.health > 0 && fighter2.health > 0) {
        const damage1 = fighter2.getHitPower() - fighter1.getBlockPower();
        fighter1.health -= (damage1 > 0 ? damage1 : 0);

        const barWidth1 = (fighter1.health / health1) * 100;
        const hitWidth1 = (damage1 / health1) * 100;

        const damage2 = fighter1.getHitPower() - fighter2.getBlockPower();
        fighter2.health -= (damage2 > 0 ? damage2 : 0);

        const barWidth2 = (fighter2.health / health2) * 100;
        const hitWidth2 = (damage2 / health2) * 100;

        this._updateHpBars(barWidth1, hitWidth1, barWidth2, hitWidth2);
      } else {
        clearInterval(interval);
        callback(fighter1.health > 0 ? fighter1 : fighter2);
      }
    }, 1000);
  }

  _handleClickFight(fightersView) {
    const fighters = fightersView.fight.map((fighter) =>
      new Fighter(fightersView.fightersDetailsMap.get(fighter._id)));

    document.getElementById('start-button').disabled = true;
    document.getElementById('hp').style.display = 'block';
    const winAudio = new Audio("../../resources/sounds/victory.mp3");
    const startAudio = new Audio("../../resources/sounds/round1.mp3");
    startAudio.play();

    this._fight(...fighters, winner => {
      this._displayWinner(winner);
      winAudio.play();
    });
  }

  _inizializeHpBars(fighter1, fighter2) {
    document.getElementById('name1').innerText = fighter1.name;
    document.getElementById('name2').innerText = fighter2.name;
    document.getElementById('bar1').style.width = '100%';
    document.getElementById('bar2').style.width = '100%';
    document.getElementById('winnerName').style.display = 'none';
  }

  _updateHpBars(barWidth1, hitWidth1, barWidth2, hitWidth2) {
    const bar1 = document.getElementById('bar1');
    const hit1 = document.getElementById('hit1');
    const bar2 = document.getElementById('bar2');
    const hit2 = document.getElementById('hit2');
    hit1.style.width = `${hitWidth1}%`;
    hit2.style.width = `${hitWidth2}%`;
    setTimeout(() => {
      hit1.style.width = 0;
      bar1.style.width = `${barWidth1 < 0 ? 0 : barWidth1}%`;
      hit2.style.width = 0;
      bar2.style.width = `${barWidth2 < 0 ? 0 : barWidth2}%`;
    }, 500);
  }

  _displayWinner(winner) {
    const winnerName = document.getElementById('winnerName');
    winnerName.style.display = 'block';
    winnerName.innerText = `Winner is ${winner.name}!`;
    if (this._choosed.length === 2)
      document.getElementById('start-button').disabled = false;
  }
}

export default fightController;