function showDetails(details, callback) {

  function showCover() {
    const coverDiv = document.createElement('div');
    coverDiv.id = 'cover-div';
    document.body.appendChild(coverDiv);
  }

  function hideCover() {
    document.body.removeChild(document.getElementById('cover-div'));
  }

  function complete(details) {
    hideCover();
    container.style.display = 'none';
    callback(details);
  }

  const inputs = [];
  const form = document.getElementById('details-form');
  const container = document.getElementById('details-form-container');
  showCover();

  for (const key in details) {
    const element = document.getElementById(key);
    if (element) {
      element.value = details[key];
      inputs.push(element);
    }
  }

  form.onsubmit = function() {
    inputs.forEach((input) => details[input.id] = input.value);
    complete(details);
    return false;
  };
  container.style.display = 'block';
}

export default showDetails;
