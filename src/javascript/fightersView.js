import View from './view';
import FighterView from './fighterView';
import { fighterService } from './services/fightersService';
import showDetails from './showDetails'
class FightersView extends View {
  constructor(fighters) {
    super();
    
    this.handleClick = this.handleFighterClick.bind(this);
    this.createFighters(fighters);

    this.fight = [];
  }

  fightersDetailsMap = new Map();

  createFighters(fighters) {
    const fighterElements = fighters.map(fighter => {
      const fighterView = new FighterView(fighter, this.handleClick);
      return fighterView.element;
    });
    this.fighterElements = fighterElements;
    this.element = this.createElement({ tagName: 'div', className: 'fighters'});
    this.element.append(...fighterElements);
  }
  _chooseFighter(fighter){
    const defaultHEIGHT = '260px';
    const selectedHEIGHT = '320px';
    const borderSTYLE = '3px solid yellow';

    const imageElements = document.getElementsByClassName('fighter-image');
    const id = fighter._id - 1;

    function select(id, fighter){
      this.fight.push(fighter);
      this.fighterElements[id].style.border = borderSTYLE;
      imageElements[id].style.height = selectedHEIGHT;
    }
    select = select.bind(this);

    function unselect(id, fighter){
      if(fighter)
        this.fight.splice(this.fight.indexOf(fighter), 1);
      this.fighterElements[id].style.border = '';
      imageElements[id].style.height = defaultHEIGHT;
    }
    unselect = unselect.bind(this);

    if(!fighter.selected){
      if(this.fight.length === 2){ 
        const f1 = this.fight.shift();
        const id = f1._id - 1;
        unselect(id);
        f1.selected = !f1.selected;
      }
      select(id, fighter);
    }
    else
      unselect(id, fighter);
    fighter.selected = !fighter.selected;
  }
  async handleFighterClick(event, fighter) {
    let details = this.fightersDetailsMap.get(fighter._id);
    if(!details){
      details = await fighterService.getFighterDetails(fighter._id)
      fighter.selected = false;
    }
    !fighter.selected ? showDetails(details, function(updatedDetails) {
      details = updatedDetails;
    }) : 0;
    this.fightersDetailsMap.set(fighter._id, details);
    this._chooseFighter(fighter);
    this.fight.length === 2 ?
      document.getElementById('start-button').disabled = false
      : document.getElementById('start-button').disabled = true;
  }
}

export default FightersView;